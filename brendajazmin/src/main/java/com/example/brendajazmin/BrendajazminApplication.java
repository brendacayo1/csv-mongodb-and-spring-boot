package com.example.brendajazmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrendajazminApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrendajazminApplication.class, args);
	}

}
