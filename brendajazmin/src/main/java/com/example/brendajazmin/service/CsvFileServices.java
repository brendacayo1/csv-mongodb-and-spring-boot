package com.example.brendajazmin.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.brendajazmin.model.Customer;
import com.example.brendajazmin.repository.CustomerRepository;
import com.example.brendajazmin.util.ApacheCommonsCsvUtil;


@Service
public class CsvFileServices {

	@Autowired
	CustomerRepository customerRepository;

	// Store Csv File's data to database
	public void store(InputStream file) {
		try {
			
			List<Customer> lstCustomers = ApacheCommonsCsvUtil.parseCsvFile(file);
			customerRepository.saveAll(lstCustomers);
		} catch(Exception e) {
			throw new RuntimeException("FAIL! -> message = " + e.getMessage());
		}
	}
	
	// Load Data to CSV File
    public void loadFile(Writer writer) throws IOException {
    	try {
        	List<Customer> customers = (List<Customer>) customerRepository.findAll();
             ApacheCommonsCsvUtil.customersToCsv(writer, customers);        	 		
    	} catch(Exception e) {
    		throw new RuntimeException("Fail! -> Message = " + e.getMessage());
    	}
    }
	
}
