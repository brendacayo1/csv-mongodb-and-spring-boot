package com.example.brendajazmin.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.brendajazmin.model.Customer;


public interface CustomerRepository extends MongoRepository<Customer, String>{

}
